## Videos

### [JetBrainsTV](https://www.youtube.com/channel/UCGp4UBwpTNegd_4nCpuBcow)
- [JetBrains Rider - New Cross-Platform .NET IDE Overview](https://www.youtube.com/watch?v=xkPtX492IhI)
- [What can Rider do for me?](https://www.youtube.com/watch?v=w8Uoyl-A2GE)
- [Unit Testing - Rider Essentials](https://www.youtube.com/watch?v=lr0I5w2apSI)
- [Be More Productive with JetBrains Rider](https://www.youtube.com/watch?v=mTW_BUUKKRM)

### Other
- [Jetbrains Rider IDE - Introduction and Tips and Tricks - Dan Clarke](https://www.youtube.com/watch?v=3p2l4FeOa2U)
