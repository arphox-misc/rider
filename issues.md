## Bugs
- [~~Microsoft Defender configuration warning even if everything is configured~~](https://youtrack.jetbrains.com/issue/RIDER-93337)
- [Unhandled exception window location and call stack is inconvenient](https://youtrack.jetbrains.com/issue/RIDER-93126)
- ⏳[~~Space before '/>' style in csproj files is handled inconsistently~~](https://youtrack.jetbrains.com/issue/RIDER-85788) -> [ .csproj files contain whitespace modifications after random actions](https://youtrack.jetbrains.com/issue/RIDER-40468/)
- [Ctrl+Alt+N shortcut sometimes stops working in Hungarian keyboard layout](https://youtrack.jetbrains.com/issue/RIDER-77528/)
- [Opening "Most recently opened tab" on closing the current tab is not working if navigated from a pinned tab](https://youtrack.jetbrains.com/issue/RIDER-74515)
- [IDE Settings Sync overridden settings to older keymaps and not synced window layout](https://youtrack.jetbrains.com/issue/IDEA-288426)
- [Structure tool window is empty for decompiled code](https://youtrack.jetbrains.com/issue/RIDER-28196)
- [Exception filters are evaluated twice in debug mode if the filter returns false](https://youtrack.jetbrains.com/issue/RIDER-78374)

## Features
- [Code Vision VCS Info (git) metric enhancement (show last editor name instead of creator)](https://youtrack.jetbrains.com/issue/RIDER-77486)
- [Preview tabs for regular navigation in code](https://youtrack.jetbrains.com/issue/RIDER-74513)
- [Deleting newline from ahead of a comment line should merge the comment with the previous line](https://youtrack.jetbrains.com/issue/RIDER-91082)

### Missing major features
- [**Modules** view is missing](https://youtrack.jetbrains.com/issue/RIDER-18260)

## Inconvenience
- [Project diagram should have a shortcut for hiding a project](https://youtrack.jetbrains.com/issue/RIDER-75250)
- [Continuous Testing inconvenience](https://www.jetbrains.com/help/rider/Work_with_Continuous_Testing.html) - see comments

## Improvement

## Fixed
- [Cannot select error message content](https://youtrack.jetbrains.com/issue/RIDER-75197)
- [~~Extract method has no effect if a constant is present~~](https://youtrack.jetbrains.com/issue/RIDER-74493~~) -> [Extract Method: const variable is selected in Parameters table and refactoring does nothing.](https://youtrack.jetbrains.com/issue/RSRP-487090)
- [The "Remove" item from the "Bookmarks" toolbar doesn't work.](https://youtrack.jetbrains.com/issue/RIDER-74485) - nem én jelentettem be de én is tapasztaltam

## Shelved
- [Enable the `Filter members by [EditorBrowsable] attribute` setting by default](https://youtrack.jetbrains.com/issue/RIDER-82644/) -> [EditorBrowsable is ignored by default](https://youtrack.jetbrains.com/issue/RSRP-330352)
- [Make "Inherit values from" setting editable](https://youtrack.jetbrains.com/issue/RIDER-75798)

## Answered
- [Cannot import live template exported by Resharper](https://youtrack.jetbrains.com/issue/RIDER-75196) - a bit like a bug
