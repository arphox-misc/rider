(c) meaning: custom hotkey  
The **bolded** names are names in the Rider Keymap settings.

## Hotkeys I use

### File
- [**Settings**](https://www.jetbrains.com/help/rider/Rider_Settings.html?keymap=primary_visual_studio) - `Ctrl+Alt+S`

### View
- [**Quick Documentation**](https://www.jetbrains.com/help/rider/Coding_Assistance__Quick_Documentation.html?keymap=primary_intellij) - `Ctrl+Q` (IJ)
- [**Parameter Info**](https://www.jetbrains.com/help/rider/Coding_Assistance__Parameter_Info.html) - `Ctrl+Shift+Space` (VS), `Ctrl+P` (IJ)

### Navigate
- [**Back**](https://www.jetbrains.com/help/rider/Navigation_and_Search__Navigating_to_Recent_Locations.html#back_forward) - `Alt+Left` (c)
- [**Forward**](https://www.jetbrains.com/help/rider/Navigation_and_Search__Navigating_to_Recent_Locations.html#back_forward) - `Alt+Right` (c)
- **Scroll Up** - `Ctrl+Up`
- **Scroll Down** - `Ctrl+Down`
- [**Search Everywhere**](https://www.jetbrains.com/help/rider/Searching_Everywhere.html?keymap=primary_visual_studio) - `Shift, Shift`, `Ctrl+T`
- [**Go to File**](https://www.jetbrains.com/help/rider/Navigation_and_Search__Go_to_File.html) - `Ctrl+Shift+T`
- [**Go to Symbol**](https://www.jetbrains.com/help/rider/Navigation_and_Search__Go_to_Symbol.html) - `Ctrl+Alt+Shift+T`
- [**Go to Line:Column**](https://www.jetbrains.com/help/rider/Navigation_and_Search__Context_Dependent_Navigation.html#navigating-code-lines-by-numbers) - `Ctrl+G`
- [**File Member** (go to)](https://www.jetbrains.com/help/rider/Navigation_and_Search__Go_to_File_Member.html): `Alt+F`(c)
- [**Navigate To..**](https://www.jetbrains.com/help/rider/Navigation_and_Search__Navigate_from_Here.html) - `Alt+N`(c) - in code, "navigate to" context menu -> declaration, implementation, usages, etc.
- **Previous Method** - `Alt+Up` (VS)
- **Next Method** - `Alt+Down` (VS)
- **Select In...** - `Alt+F1` (VS) - This is how I select a file in the Solution window that is open in the editor (like VS's Sync with Active Document shortcut). I do `Alt+F1,1`.
- **Type Hierarchy** - `Ctrl+E,H`

### Code
- [**Generate**](https://www.jetbrains.com/help/rider/Generating_Type_Members.html) - `Alt+Insert`
- [**Move Statement Down/Up**](https://www.jetbrains.com/help/rider/Coding_Assistance__Moving_Code_Elements.html) - `Ctrl+Alt+Shift+Down/Up` (VS), `Ctrl+Shift+Down/Up` (IJ)
- [**Move Element Left/Right**](https://www.jetbrains.com/help/rider/Coding_Assistance__Moving_Code_Elements.html) - `Ctrl+Alt+Shift+Left/Right`

### Refactor
- [**Change Signature...**](https://www.jetbrains.com/help/rider/Refactorings__Change_Signature.html) - `Ctrl+R,S` (VS)
- [**Inline**](https://www.jetbrains.com/help/rider/Inline_refactorings.html) - `Ctrl+Alt+N` (IJ), `Ctrl+R,I` (VS)
- [**Introduce Variable...**](https://www.jetbrains.com/help/rider/Refactorings__Introduce_Variable.html) - `Ctrl+Alt+V` (IJ), `Ctrl+R,V` (VS)
- [**Introduce Parameter...**](https://www.jetbrains.com/help/rider/Refactorings__Introduce_Parameter.html) - `Ctrl+Alt+P` (IJ), `Ctrl+R,P` (VS)
- [**Extract Method...**](https://www.jetbrains.com/help/rider/Refactorings__Extract_Method.html) - `Ctrl+Alt+M` (IJ), `Ctrl+R,M` (VS)

### Build
- [**Build Solution**](https://www.jetbrains.com/help/rider/Building_Projects.html) - `F6` (c)

### Run / Debug
- [**Resume Program**](https://www.jetbrains.com/help/rider/Starting_the_Debugger_Session.html#resume-execution) - `F5` (VS). If debugging, resumes. If not debugging, starts debugging.
- [**Evaluate Expression**](https://www.jetbrains.com/help/rider/Evaluating_Expressions.html#eval-expression-dialog) - `Shift+F9`
- [**Run to Cursor**](https://www.jetbrains.com/help/rider/Stepping_Through_the_Program.html) - `Ctrl+F10` (VS)

### Tests
- [**Run Unit Tests**](https://www.jetbrains.com/help/rider/Unit_Testing_in_Document.html) - `Ctrl+U,R` / `Ctrl+U,Ctrl+R` (VS)
- [**Debug Unit Tests**](https://www.jetbrains.com/help/rider/Unit_Testing_in_Document.html) - `Ctrl+U,D` / `Ctrl+U,Ctrl+D` (VS)
- [**Repeat Previous Run**](https://www.jetbrains.com/help/rider/Unit_Testing_in_Document.html) - `Ctrl+U,U` / `Ctrl+U,Ctrl+U` (VS)
- [**Run Current Session**](https://www.jetbrains.com/help/rider/Executing_Analyzing_Tests.html) - `Ctrl+U,Y` / `Ctrl+U,Ctrl+Y` (VS)
- [**Run All Tests from Solution**](https://www.jetbrains.com/help/rider/Unit_Testing_in_Solution.html) - `Ctrl+U,L` / `Ctrl+U,Ctrl+L` (VS)

### Window
- [**Hide Active Tool Window**](https://www.jetbrains.com/help/rider/Tool_Windows.html?keymap=primary_visual_studio) - `Shift+Escape`
- **Close Tab** - `Alt+X` (c) (easier to reach than Ctrl+F4)

### Tool Windows
- [**Unit Tests**](https://www.jetbrains.com/help/rider/Unit_Testing_in_Solution.html) - `Alt+0` (c)
- [**Explorer**](https://www.jetbrains.com/help/rider/Managing_Projects_and_Solutions.html?keymap=primary_intellij) - `Alt+1` ("Explorer")
- [**Bookmarks**](https://www.jetbrains.com/help/rider/Bookmarks.html?keymap=primary_visual_studio) - `Alt+2`
- [**Find**](https://www.jetbrains.com/help/rider/Navigation_and_Search__Finding_Usages__Viewing_Filtering_and_Grouping_Results.html) - `Alt+3`
- [**Structure**](https://www.jetbrains.com/help/rider/Navigation_and_Search__Context_Dependent_Navigation.html#navigating-file-structure) - `Ctrl+Alt+F`
- [**Stretch to Left**/Right/Up/Down](https://www.jetbrains.com/help/rider/Manipulating_the_Tool_Windows.html) - `Ctrl+Alt+Shift+Left/Right/Up/Down` - to resize tool windows / increase/decrease their size.

### Other
- [**Previous Error**](https://www.jetbrains.com/help/rider/Design_time_Inspection.html?keymap=primary_intellij#navigating_errors) - `Alt+Shift+F2` or `Alt+Shift+Up`
- [**Next Error**](https://www.jetbrains.com/help/rider/Design_time_Inspection.html?keymap=primary_intellij#navigating_errors) - `Alt+F2` or `Alt+Shift+Down`
- **NewElement**: `Alt+Insert` - Create new element in the folder in Explorer

## Hotkeys to learn?
- [**Recent Files**](https://www.jetbrains.com/help/rider/Navigation_and_Search__Navigating_to_Recent_Locations.html?keymap=primary_visual_studio#recent_files) - `Ctrl+Comma`
- [**Recently Changed Files**](https://www.jetbrains.com/help/rider/Navigation_and_Search__Navigating_to_Recent_Locations.html#recent_edits) - `Ctrl+Shift+Comma`
- [**Last Edit Location**](https://www.jetbrains.com/help/rider/Navigation_and_Search__Navigating_to_Recent_Locations.html#recent_edits) (Jump back to previous edit) - `Ctrl+Shift+Backspace`
- **Jump to Navigation Bar**: `Ctrl+F2` (VS)

## Tips
- You can set keybinds to tool windows (`Alt`+numbers or `Ctrl+Alt`+numbers)
- There is an [Endpoints tool window](https://www.jetbrains.com/help/rider/Endpoints_tool_window.html) to see your API endpoints.
- Remove the `Shift+Delete` keybind from the `Edit.Cut` action (so it remains `Ctrl+X`) and set it to `Delete Line`. This way using `Shift+Delete` to delete the current line doesn't overwrite the clipboard but only does what it is supposed to: simply delete a line.
- Use the **Structure** tool window to easily navigate in the current file's structure.
- Try [Structural navigation with Tab and Shift+Tab](https://www.jetbrains.com/help/rider/Navigation_and_Search_Structural_Navigation.html), and you can also [Structural Remove](https://www.jetbrains.com/help/rider/Coding_Assistance_Typing_Assistance.html#structural-remove).
